package com.imitationProject;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class CommandServiceCsv {

    private static final String HELP = "help";
    private static final String CREATE = "create";
    private static final String READ = "read";
    private static final String READALL = "readall";
    private static final String READ_ALL = "read_all";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";
    private static final String EXIT = "exit";
    private static final String SWITCH = "switch";
    private static final String GOODBYE = "Досвидания, программа завершена";
    private static final String WARNING = "!!! WARNING !!! WRONG COMMAND !!! BE CAREFUL !!!\n";
    private static final String GUIDE =
            "Список доступных Вам команд:\n" +
                    "<<create>> позволяет занести данные о новом пользователе в базу данных окружения CSV;\n" +
                    "<<read>> позволяет вычитать информацию про конкретного пользователя по его ID номеру;\n" +
                    "<<readAll>> позволяет вычитать информацию про всех пользователей внесенных в базу данных;\n" +
                    "<<update>> позволяет изменить информацию про конкретного пользователя внесенного в базу данных;\n" +
                    "<<delete>> позволяет удалить всю информацию про конкретного пользователя внесенного в базу данных;\n" +
                    "<<start>> применяет ранее внесенные изменения действия 'create', 'update', 'delete';\n" +
                    "<<switch>> сменить окружение на XML, Binary, YAML, JSON;\n" +
                    "<<help>> выводит это меню подсказок;\n" +
                    "<<exit>> завершает работу программы.\n";
    ServicesCsv services = new ServicesCsv();
    Scanner scanner = new Scanner(System.in);
    String command;

    public void userInterface() throws IOException {

        System.out.println("Вы в меню окружения CSV.\nВведите команду:");

        String input = scanner.next();

        command = input.toLowerCase(Locale.ROOT);

        switch (command) {
            case CREATE:
                services.create();
                userInterface();
                break;
            case UPDATE:
                services.updateByID();
                userInterface();
                break;
            case DELETE:
                services.deleteByID();
                userInterface();
                break;
            case READALL:
                services.readAll();
                userInterface();
                break;
            case READ_ALL:
                services.readAll();
                userInterface();
                break;
            case READ:
                services.readByID();
                break;
            case SWITCH:
                services.switchExtension();
                userInterface();
                break;
            case HELP:
                System.out.println(GUIDE);
                userInterface();
                break;
            case EXIT:
                System.out.println(GOODBYE);
                System.exit(0);
                break;
            default:
                System.out.println(WARNING + GUIDE);
                userInterface();
                break;
        }
    }
}
