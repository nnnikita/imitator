package com.imitationProject;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

public class ServicesJson {

    Scanner scanner = new Scanner(System.in);
    ObjectMapper mapper = new ObjectMapper();

    public void readAll() throws IOException {
        PeopleJson[] people = mapper.readValue(Paths.get("people.json").toFile(), PeopleJson[].class);
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i]);
        }
        System.out.println();
    }

    public void readByID() throws IOException {
        PeopleJson[] people = mapper.readValue(Paths.get("people.json").toFile(), PeopleJson[].class);
        System.out.println("Введите ID человека, информацию о котором хотите вычитать:");
        String inputId = scanner.next();
        boolean trigger = true;
        for (int i = 0; i < people.length; i++) {
            if (inputId.equals(people[i].id)) {
                System.out.println(people[i]);
                trigger = false;
                new CommandServiceJson().userInterface();
            }
        }
        if (trigger == true) {
            System.out.println("Нет такого номера ID");
            new CommandServiceJson().userInterface();
        }
        System.out.println();
    }

    public void deleteByID() throws IOException {
        PeopleJson[] people = mapper.readValue(Paths.get("people.json").toFile(), PeopleJson[].class);
        System.out.println("Введите ID человека, информацию о котором хотите удалить:");
        String inputId = scanner.next();
        PeopleJson[] afterDel = null;
        boolean trigger = false;
        for (int i = 0; i < people.length; i++) {
            if (inputId.equals(people[i].id)) {
                afterDel = new PeopleJson[people.length - 1];
                for (int index = 0; index < i; index++) {
                    afterDel[index] = people[index];
                }
                for (int j = i; j < people.length - 1; j++) {
                    afterDel[j] = people[j + 1];
                }
                trigger = true;
            }
        }
        if (trigger == false) {
            System.out.println("Нет такого номера ID");
        }
        if (trigger == true) {
            System.out.println();
            start(afterDel);
        }
    }

    public void create() throws IOException {
        String newID;
        PeopleJson[] people = mapper.readValue(Paths.get("people.json").toFile(), PeopleJson[].class);
        System.out.println("Введите <<ID>> новой записи:");
        newID = scanner.next();
        checkID(people,newID);
        System.out.println("Введите <<firstName>> новой записи:");
        String newFirstName = scanner.next();
        System.out.println("Введите <<lastName>> новой записи:");
        String newLastName = scanner.next();
        System.out.println("Введите <<age>> новой записи:");
        String newAge = scanner.next();
        System.out.println("Введите <<city>> новой записи:");
        String newCity = scanner.next();

        PeopleJson[] afterCreate = new PeopleJson[people.length + 1];
        afterCreate[afterCreate.length - 1] = new PeopleJson();

        afterCreate[afterCreate.length - 1].id = newID;
        afterCreate[afterCreate.length - 1].firstName = newFirstName;
        afterCreate[afterCreate.length - 1].lastName = newLastName;
        afterCreate[afterCreate.length - 1].age = newAge;
        afterCreate[afterCreate.length - 1].city = newCity;

        for (int i = 0; i < people.length; i++) {
            afterCreate[i] = people[i];
        }
        start(afterCreate);
    }

    public void updateByID() throws IOException {
        PeopleJson[] people = mapper.readValue(Paths.get("people.json").toFile(), PeopleJson[].class);
        System.out.println("Введите ID человека, информацию о котором хотите изменить:");
        String inputId = scanner.next();
        boolean trigger = false;
        for (int i = 0; i < people.length; i++) {
            if (inputId.equals(people[i].id)) {
                System.out.println("Введите <<ID>> новой записи");
                people[i].id = scanner.next();
                System.out.println("Введите <<firstName>> новой записи");
                people[i].firstName = scanner.next();
                System.out.println("Введите <<lastName>> новой записи");
                people[i].lastName = scanner.next();
                System.out.println("Введите <<age>> новой записи");
                people[i].age = scanner.next();
                System.out.println("Введите <<city>> новой записи");
                people[i].city = scanner.next();
                trigger = true;
            }
        }
        if (trigger == false) {
            System.out.println("Нет такого ID");
        }
        System.out.println();
        if (trigger == true) {
            System.out.println();
            start(people);
        }
    }

    public void switchExtension() throws IOException {
        System.out.println("Выберете одно из расширений в окружение которого хотите перейти <<XML>>, <<CSV>>, <<BIN>>, <<YAML>>");
        String command = scanner.next();
        command = command.toLowerCase(Locale.ROOT);
        switch (command) {
            case "xml":
                new CommandServiceXml().userInterface();
                break;
            case "csv":
                new CommandServiceCsv().userInterface();
                break;
            case "bin":
                new CommandServiceBin().userInterface();
                break;
            case "yaml":
                new CommandServiceYaml().userInterface();
                break;
            case "json":
                new CommandServiceJson().userInterface();
                break;
            case "exit":
                System.exit(0);
                break;
            default:
                System.out.println("Неверная команда.");
                new CommandServiceJson().userInterface();
                break;
        }
    }

    private void start(PeopleJson[] array) throws IOException {
        System.out.println("Введите <<start>> если хотите сохранить изменение, <<exit>> для выхода из програмы.");
        String command = scanner.next();
        command = command.toLowerCase(Locale.ROOT);
        switch (command) {
            case "start":
                mapper.writeValue(Paths.get("people.json").toFile(), array);
                System.out.println("Изменения сохранены, Вы в меню окружения JSON.");
                break;
            case "exit":
                System.exit(0);
                break;
            default:
                System.out.println("Неверная команда, изменения НЕ сохранены, Вы в меню окружения JSON.");
                new CommandServiceJson().userInterface();
                break;
        }
    }

    private void checkID(PeopleJson[] array, String id) throws IOException {
        for (int i = 0; i < array.length; i++) {
            if (id.equals(array[i].id)) {
                System.out.println("Такой ID уже есть, введите другой");
                new CommandServiceJson().userInterface();
            }
        }
    }
}

