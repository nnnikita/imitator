package com.imitationProject;

//    @JsonAutoDetect
    public class PeopleJson {

        public String id;
        public String firstName;
        public String lastName;
        public String age;
        public String city;

        public PeopleJson() {
        }

        @Override
        public String toString() {
            return "id: " + id + "; Name: " + firstName + "; lastName: " + lastName + "; age: " + age + "; city: " + city;
        }
    }