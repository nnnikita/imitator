package com.imitationProject;

import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class MainCommandService {
    private static final String GUIDE ="Выберете одно из расширений в окружение которого хотите перейти <<XML>>, <<CSV>>, <<BIN>>, <<YAML>>, <<JSON>>";

    CommandServiceJson commandServiceJson = new CommandServiceJson();
    CommandServiceYaml commandServiceYaml = new CommandServiceYaml();
    CommandServiceCsv commandServiceCsv = new CommandServiceCsv();
    CommandServiceBin commandServiceBin = new CommandServiceBin();
    CommandServiceXml commandServiceXml = new CommandServiceXml();
    Scanner scanner = new Scanner(System.in);
    String command;

    public void processCommand() throws IOException {
        System.out.println("Вас приветствует приложение для работы с записями людей, выберите окружение для работы: ");
        command = scanner.next();
        command = command.toLowerCase(Locale.ROOT);
        switch (command) {
            case "json":
                commandServiceJson.userInterface();
                break;
            case "yaml":
                commandServiceYaml.userInterface();
                break;
            case "csv":
                commandServiceCsv.userInterface();
                break;
            case "xml":
                commandServiceXml.userInterface();
                break;
            case "bin":
                commandServiceBin.userInterface();
                break;
            case "help":
                System.out.println(GUIDE);
                System.out.println();
                processCommand();
            case "exit":
                System.exit(0);
            default:
                System.out.println("Wrong command");
        }
    }
}
